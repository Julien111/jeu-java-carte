package jeuSorciere.jeu.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import jeuSorciere.jeu.entitee.Carte;
import jeuSorciere.jeu.entitee.Joueur;
import jeuSorciere.jeu.entitee.Partie;

public class PartieService {

	private List<Partie> parties = new ArrayList<>();

	/**
	 * fonction qui va créer une partie
	 * 
	 * @param pseudo
	 * @param nomPartie
	 * @return int
	 */
	public int creerPartie(String pseudo, String nomPartie) {

		Joueur joueur = new Joueur();
		joueur.setPseudo(pseudo);

		System.out.println(joueur.getPseudo());

		// create party

		Partie partie = new Partie();
		partie.setNom(nomPartie);
		List<Joueur> listeJoueur = partie.getJoueurs();
		listeJoueur.add(joueur);
		partie.setJoueurs(listeJoueur);

		System.out.println(partie.getNom());

		// add party

		parties.add(partie);

		System.out.println("Votre partie a été créée, son id est : " + partie.getId());

		return partie.getId();
	}

	/**
	 * Fonction Lister les parties
	 * 
	 * @return List
	 */
	public List<Partie> listerParties() {
		return this.parties;
	}

	/**
	 * Trouver une partie par son id
	 * 
	 * @param id
	 * @return Partie
	 */
	public Partie findPartieById(int id) {
		Partie thePartie = null;
		for (Partie partie : parties) {
			if (partie.getId() == id) {
				thePartie = partie;
			}
		}
		if (thePartie == null) {
			System.out.println("La partie n'a pas été trouvée !");
		}
		return thePartie;
	}

	/**
	 * Fonction pour rejoindre une partie
	 * 
	 * @param idPartie
	 * @param pseudoJoueur
	 */
	public void rejoindrePartie(int idPartie, String pseudo) {

		Joueur joueur = new Joueur();
		joueur.setPseudo(pseudo);
		// find party
		Partie partie = findPartieById(idPartie);

		// add
		partie.getJoueurs().add(joueur);

		System.out.println("Vous avez rejoint la partie numéro : " + idPartie);
	}

	/**
	 * Fonction pour démarrer la partie
	 */
	public int démarrerPartie(int idPartie) {

		// return si un seul joueur
		Partie partie = findPartieById(idPartie);
		if (partie.getJoueurs().size() <= 1) {
			System.out.println("Vous êtes le seul joueur, la partie ne peut pas commencer !");
			return idPartie;
		}
		
		// distribuer les cartes
		partie.getJoueurs().forEach(joueur -> {
			for (int i = 0; i < 7; i++) {
				List<Carte> cartesDuJoueur = joueur.getCartes();
				cartesDuJoueur.add(piocherCarte());
			}
		});
		
		// donner la main au joueur qui va commencer (prendre celui qui a l'id la plus
		// faible)
		int id = 0;
		Joueur joueurMain = null;

		for (Joueur joueur : partie.getJoueurs()) {
			if (id == 0) {
				joueurMain = joueur;
				id = joueur.getId();
			} else {
				if (id > joueur.getId()) {
					joueurMain = joueur;
				}
			}
		}
		// on donne la main au joueur
		partie.setJoueurQuiALaMain(joueurMain);
		
		return idPartie;
	}

	/**
	 * Fonction pour piocher une carte de manière aléatoire.
	 */
	public Carte piocherCarte() {
		Carte carte = new Carte();
		Random rand = new Random();
		int number = rand.nextInt(4 - 1 + 1) + 1;

		switch (number) {
		case 1:
			carte.setIngredient(Carte.Ingredient.AILE_DE_CHAUVE_SOURIS);
			break;
		case 2:
			carte.setIngredient(Carte.Ingredient.BAVE_DE_CRAPAUD);
			break;
		case 3:
			carte.setIngredient(Carte.Ingredient.MANDRAGORE);
			break;
		case 4:
			carte.setIngredient(Carte.Ingredient.LAPIS_LAZULIS);
			break;
		}

		return carte;
	}
	
	//fonction à utiliser lorsqu'on commence la partie
	
	/**
	 * fonction pour lister les cartes d'un joueur
	 * @param idPartie
	 * @param idJoueur
	 * @return List Carte
	 */
	public List<Carte> listerCarteJoueur(int idPartie, int idJoueur) {
		Partie partie = findPartieById(idPartie);
		Joueur joueur = null;
		
		for(Joueur findJoueur : partie.getJoueurs()) {
			if(findJoueur.getId() == idJoueur) {
				joueur = findJoueur;
			}
		}		
		return joueur.getCartes();		
	}
	
	
	
	/**
	 * Fonction pour passer son tour
	 * le joueur pioche une carte
	 * la main change et passe au joueur suivant
	 */
	public void passerTour(int idPartie, int idJoueur) {
		
		Partie partie = findPartieById(idPartie);		
		
		//piocher une carte
		
		System.out.println(partie.getJoueurQuiALaMain().getPseudo());
		
		Joueur joueur = partie.getJoueurQuiALaMain();		
		List<Carte> cartesJoueur = joueur.getCartes();
		System.out.println(cartesJoueur.toString());
		cartesJoueur.add(piocherCarte());
		
		System.out.println(cartesJoueur.toString());
		
		//determiner le nombre de tour a attendre      *** que si le joueur est endormie
		joueur.setNombreDeToursAAttendre(partie.getJoueurs().size()-1);
		
		//la main passe au joueur suivant
		int indexJoueurMain = partie.getJoueurs().indexOf(joueur);		
		int tailleListeJoueur = partie.getJoueurs().size() - 1;
		
		if(indexJoueurMain >= tailleListeJoueur){
			Joueur newJoueurMain = partie.getJoueurs().get(0);
			partie.setJoueurQuiALaMain(newJoueurMain);
		}
		else {
			indexJoueurMain += 1;
			Joueur newJoueurMain = partie.getJoueurs().get(indexJoueurMain);
			partie.setJoueurQuiALaMain(newJoueurMain);
		}		
		//System.out.println(partie.getJoueurQuiALaMain().getPseudo());		
	}
	
	/**
	 * Trouver un joueur par son id
	 * @param id
	 * @return Joueur
	 */
	public Joueur findJoueurById(int id, int idPartie) {
		Partie partie = findPartieById(idPartie);		
		Joueur joueur = partie.getJoueurs().stream()
				  .filter(playeur -> playeur.getId() == id)
				  .findAny()
				  .orElse(null);		
		return joueur;		
	}
	
	/**
	 * Prendre une carte au hasard dans une liste d'un adversaire
	 * @param idPartie
	 * @param idVoleur
	 * @param idVictime
	 */
	public void prendreCarteAuHasard(int idPartie, int idVoleur, int idVictime) {
		Partie partie = findPartieById(idPartie);		
		Joueur voleur = findJoueurById(idVoleur, partie.getId());
		Joueur victime = findJoueurById(idVictime, partie.getId());
		
		//lister les cartes de la victime et récupérer une carte aléatoire
		
		List<Carte> cartesVictime = victime.getCartes();
		int nombreCartesVictime = cartesVictime.size() - 1;
		
		Random rand = new Random();
		int number = rand.nextInt(nombreCartesVictime - 0 + 0) + 0;
		Carte carte = cartesVictime.get(number);
		
		//ajouter la carte chez le voleur
		
		List<Carte> listeCarteVoleur= voleur.getCartes();
		listeCarteVoleur.add(carte);		
		voleur.setCartes(listeCarteVoleur);		
		
		//enlever la carte chez la victime
		cartesVictime.remove(number);	
	}
	
	// ** Liste des sortilèges **
	
	/**
	 * Le joueur prend une carte chez tous les adversaires
	 */
	public void sortInvisibilite(int idPartie) {
		Partie partie = findPartieById(idPartie);	
		
		//on doit avoir l'id de la personne qui lance le sortilège
		Joueur joueurMain = partie.getJoueurQuiALaMain();
		
		//on va devoir boucler pour trouver les joueurs a qui enlever une carte
		
		for(Joueur joueur : partie.getJoueurs()) {
			if(joueur.getId() == joueurMain.getId()) {
				continue;
			}
			else {
				prendreCarteAuHasard(partie.getId(), joueurMain.getId(), joueur.getId());
			}
		}
	}
	
	/**
	 * Le joueur de votre choix vous donne la moitié de ses cartes(au hasard). 
	 * S’il ne possède qu’une carte il a perdu.
	 * @param idPartie
	 * @param idVictime
	 */
	public void sortPhiltreAmour(int idPartie, int idVictime) {
		Partie partie = findPartieById(idPartie);		
		Joueur victime = findJoueurById(idVictime, partie.getId());
		
		
	}
	
	// ** fin liste des sortilèges **
}
