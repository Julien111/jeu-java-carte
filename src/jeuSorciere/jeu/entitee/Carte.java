package jeuSorciere.jeu.entitee;

public class Carte {
	
	private int id;	
	
	private Ingredient ingredient;	

	public enum Ingredient{
		CORNE_LICORNE,
		MANDRAGORE,
		BAVE_DE_CRAPAUD,
		LAPIS_LAZULIS,
		AILE_DE_CHAUVE_SOURIS,
	}
	
private static int COMPTEUR_ID = 1;
	
	public Carte() {
		id = COMPTEUR_ID;
		COMPTEUR_ID++;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Ingredient getIngredient() {
		return ingredient;
	}

	public void setIngredient(Ingredient ingredient) {
		this.ingredient = ingredient;
	}

	@Override
	public String toString() {
		return "Carte [id=" + id + ", ingredient=" + ingredient + "]";
	}	
	
}
