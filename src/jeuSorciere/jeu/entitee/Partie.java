package jeuSorciere.jeu.entitee;

import java.util.ArrayList;
import java.util.List;

public class Partie {
	
	private int id;
	
	private String nom;
	
	private List<Joueur> joueurs = new ArrayList<>();
	
	private Joueur joueurQuiALaMain;	

	private static int COMPTEUR_ID = 1;
	
	public Partie() {
		id = COMPTEUR_ID;
		COMPTEUR_ID++;
	}

	public int getId() {
		return id;
	}	

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}	

	public List<Joueur> getJoueurs() {
		return joueurs;
	}

	public void setJoueurs(List<Joueur> joueurs) {
		this.joueurs = joueurs;
	}

	public Joueur getJoueurQuiALaMain() {
		return joueurQuiALaMain;
	}

	public void setJoueurQuiALaMain(Joueur joueurQuiALaMain) {
		this.joueurQuiALaMain = joueurQuiALaMain;
	}

	@Override
	public String toString() {
		return "Partie [id=" + id + ", nom=" + nom + ", joueurs=" + joueurs + ", joueurQuiALaMain=" + joueurQuiALaMain
				+ "]";
	}	
	
}
