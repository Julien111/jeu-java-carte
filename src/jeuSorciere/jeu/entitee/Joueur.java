package jeuSorciere.jeu.entitee;

import java.util.ArrayList;
import java.util.List;

public class Joueur {
	
	private int id;
	
	private String pseudo;	
	
	private int nombreDeToursAAttendre;
	
	private List<Carte> cartes = new ArrayList<>();	
	
	private static int COMPTEUR_ID = 1;
		
	public Joueur() {
		id = COMPTEUR_ID;
		COMPTEUR_ID++;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public int getNombreDeToursAAttendre() {
		return nombreDeToursAAttendre;
	}

	public void setNombreDeToursAAttendre(int nombreDeToursAAttendre) {
		this.nombreDeToursAAttendre = nombreDeToursAAttendre;
	}

	public List<Carte> getCartes() {
		return cartes;
	}

	public void setCartes(List<Carte> cartes) {
		this.cartes = cartes;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.getPseudo() + " " + "son id " + this.getId() + " " + this.getCartes().toString();
	}
	
}
