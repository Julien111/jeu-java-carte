package jeuSorciere.jeu.main;

import java.util.List;
import java.util.Scanner;

import jeuSorciere.jeu.entitee.Joueur;
import jeuSorciere.jeu.entitee.Partie;
import jeuSorciere.jeu.service.PartieService;

public class MainJeuSorciere {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PartieService service = new PartieService();
		service.creerPartie("joueur1", "partie1");		
		service.creerPartie("marc", "partie2");
		service.creerPartie("john", "partie3");
				
		//jeu à la console
		System.out.println("Bienvenue dans MAGIE-MAGIE");
		
		//les variables importantes
		Scanner scan = new Scanner(System.in);		
		boolean noStop = false;
		
		while(!noStop) {
			
			System.out.println("Que voulez vous faire ?");
			System.out.println("Tapez 1 :  si vous voulez créer une nouvelle partie ?");
			System.out.println("Tapez 2 :  si vous voulez lister les parties non démarrées ?");
			System.out.println("Tapez 3 : si vous voulez rejoindre une partie ?");
			System.out.println("Tapez 4 : si vous voulez démarrer une partie ?");
			System.out.println("Tapez 5 : si vous voulez quitter le jeu ?");
			System.out.println("Votre choix ?");
			int choix = scan.nextInt();
			
			if(choix == 1) {
				System.out.println("Choisissez votre pseudo ?");
				String pseudo = scan.next();
				
				System.out.println("Choisissez le nom de votre partie ?");
				String partie = scan.next();
				
				System.out.println(pseudo + " " + partie);
				service.creerPartie(pseudo, partie);				
			}
			else if(choix == 2) {
				System.out.println("La liste des parties est : ");
				List<Partie> parties = service.listerParties();
				
				for (Partie partie : parties) {
					System.out.println("Nom de la partie : " + partie.getNom());
					System.out.println("Id de la partie : " + partie.getId());
					System.out.println("La liste des joueurs qui participent à cette partie sont : ");
					for(Joueur joueur : partie.getJoueurs()) {
						System.out.println("Pseudo joueur : " + joueur.getPseudo());
					}
				}
			}
			else if(choix == 3) {
				System.out.println("Tapez l'id de la partie que vous voulez rejoindre ?");
				int id  = scan.nextInt();
				System.out.println("Veuillez écrire votre pseudo ?");
				String pseudo  = scan.next();
				
				service.rejoindrePartie(id, pseudo);
				
			}
			else if(choix == 4) {
				System.out.println("Quel est l'id de la partie que vous voulez démarrer ?");
				int idPartie  = scan.nextInt();
				
				service.démarrerPartie(idPartie);
				
				Partie partie = service.findPartieById(idPartie);
				partie.getJoueurs().toString();
				
				service.findJoueurById(4, idPartie);
				
				//System.out.println( service.findPartieById(idPartie) );
				
			}
			else if(choix == 5) {
				System.out.println("Vous avez quitté le jeu. A la prochaine.");
				noStop = true;
			}
			else {
				System.out.println("Le numéro est invalide.");
			}
		}
		
		//on ferme le scan
		
		scan.close();
		
	}

}
